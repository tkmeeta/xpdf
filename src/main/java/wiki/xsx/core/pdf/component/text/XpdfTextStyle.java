package wiki.xsx.core.pdf.component.text;

/**
 * 文本样式枚举
 * @author xsx
 * @date 2020/3/3
 * @since 1.8
 */
public enum XpdfTextStyle {
    /**
     * 居中
     */
    CENTER,
    /**
     * 居左
     */
    LEFT,
    /**
     * 居右
     */
    RIGHT;
}
